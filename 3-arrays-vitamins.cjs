const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];


/*

    1. Get all items that are available 
    2. Get all items containing only Vitamin C.
    3. Get all items containing Vitamin A.
    4. Group items based on the Vitamins that they contain in the following format:
        {
            "Vitamin C": ["Orange", "Mango"],
            "Vitamin K": ["Mango"],
        }
        
        and so on for all items and all Vitamins.
    5. Sort items based on number of Vitamins they contain.

    NOTE: Do not change the name of this file

*/ 

//     1. Get all items that are available 
function getAvailableItems(items) {
    return items.filter((item) => {
        return item.available
    })
}

const availableItems = getAvailableItems(items)
// console.log(availableItems)

//    2. Get all items containing only Vitamin C.
function getItemsByOnlyC(items,contains) {
    return items.filter((item) => {
        return item.contains === contains
    })
}

const itemContainingC = getItemsByOnlyC(items, "Vitamin C")
// console.log(itemContainingC)

//     3. Get all items containing Vitamin A.
function getItemsByContaining(items,contains) {
    return items.filter((item) => {
        return item.contains.includes(contains)
    })
}

const itemContainingA = getItemsByContaining(items, "Vitamin A")
// console.log(itemContainingA)

//     4. Group items based on the Vitamins that they contain
function getItemsByVitamins(items) {
    let vitaminObj = {}

    const vitamins = items.map((item) => {
        return item.contains.split(', ')
    })
    .flat()
    .filter((vitamin, index, arr) => {
        return arr.indexOf(vitamin) === index
    })
   
    vitaminObj = vitamins.reduce((accu, vitamin) => {
        if(accu[vitamin] === undefined) {
            accu[vitamin] = []
        }
        
        accu[vitamin] = items.reduce((accu1, curr1) => {
            if(curr1.contains.includes(vitamin)) {
                accu1.push(curr1.name)
            }
            return accu1
        },[])

        return accu
    }, {})

    return vitaminObj
}

const itemsByVitamins = getItemsByVitamins(items)
// console.log(itemsByVitamins)

//     5. Sort items based on number of Vitamins they contain.
function sortByNumberOfVitamins(items) {
    const itemsCopy = [...items]
    itemsCopy.sort((item1, item2) => {
        return item2.contains.length -item1.contains.length
    })

    return itemsCopy
}

const sortedItems = sortByNumberOfVitamins(items)
console.log(sortedItems)