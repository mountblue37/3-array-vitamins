const cards = 
[{"id":1,"card_number":"5602221055053843723","card_type":"china-unionpay","issue_date":"5/25/2021","salt":"x6ZHoS0t9vIU","phone":"339-555-5239"},
{"id":2,"card_number":"3547469136425635","card_type":"jcb","issue_date":"12/18/2021","salt":"FVOUIk","phone":"847-313-1289"},
{"id":3,"card_number":"5610480363247475108","card_type":"china-unionpay","issue_date":"5/7/2021","salt":"jBQThr","phone":"348-326-7873"},
{"id":4,"card_number":"374283660946674","card_type":"americanexpress","issue_date":"1/13/2021","salt":"n25JXsxzYr","phone":"599-331-8099"},
{"id":5,"card_number":"67090853951061268","card_type":"laser","issue_date":"3/18/2021","salt":"Yy5rjSJw","phone":"850-191-9906"},
{"id":6,"card_number":"560221984712769463","card_type":"china-unionpay","issue_date":"6/29/2021","salt":"VyyrJbUhV60","phone":"683-417-5044"},
{"id":7,"card_number":"3589433562357794","card_type":"jcb","issue_date":"11/16/2021","salt":"9M3zon","phone":"634-798-7829"},
{"id":8,"card_number":"5602255897698404","card_type":"china-unionpay","issue_date":"1/1/2021","salt":"YIMQMW","phone":"228-796-2347"},
{"id":9,"card_number":"3534352248361143","card_type":"jcb","issue_date":"4/28/2021","salt":"zj8NhPuUe4I","phone":"228-796-2347"},
{"id":10,"card_number":"4026933464803521","card_type":"visa-electron","issue_date":"10/1/2021","salt":"cAsGiHMFTPU","phone":"372-887-5974"}]

/* 

    1. Find all card numbers whose sum of all the even position digits is odd.
    2. Find all cards that were issued before June.
    3. Assign a new field to each card for CVV where the CVV is a random 3 digit number.
    4. Add a new field to each card to indicate if the card is valid or not.
    5. Invalidate all cards issued before March.
    6. Sort the data into ascending order of issue date.
    7. Group the data in such a way that we can identify which cards were assigned in which months.

    Use method chaining to solve #3, #4, #5, #6 and #7.

    NOTE: Do not change the name of this file 
*/

// 1. Find all card numbers whose sum of all the even position digits is odd.
function isEven(cards) {
    let total = 0
    function evenAdd(str, index) {
        if(index <= str.length - 1) {
            total += Number.parseInt(str.charAt(index))
            index += 2
            return evenAdd(str, index)
        }
        return total
    }

    return cards.filter((card) => {
        return evenAdd(card.card_number, 0) % 2 === 1
    })
}

// console.log(isEven(cards))

// 2. Find all cards that were issued before June.
function issuedBefore(cards, monthNumber) {
    return cards.filter((card) => {
        const month = Number.parseInt(card.issue_date.split('/')[0])
        return month < monthNumber
    })
}

// console.log(issuedBefore(cards, 6))

//  3. Assign a new field to each card for CVV where the CVV is a random 3 digit number.
function assignCVV(cards) {
    return cards.reduce((accu, card) => {
        let cardObj = {...card}
        let cvv = Math.floor(Math.random() * 1000)
        if(cvv < 100) {
            cvv += 100
        }
        cardObj['CVV'] = cvv
        accu.push(cardObj)
        return accu
    },[])
}

// console.log(assignCVV(cards))

// 4. Add a new field to each card to indicate if the card is valid or not.
function assignNewField(cards, fieldName) {
    return cards.reduce((accu, card) => {
        let cardObj = {...card}
        cardObj[fieldName] = true
        accu.push(cardObj)
        return accu
    },[])
}

// console.log(assignNewField(cards, "is_valid"))

// 5. Invalidate all cards issued before March.
function invalidBefore(cards, monthNum) {
    return cards.reduce((accu, card) => {
        let cardObj = {...card}
        const month = Number.parseInt(cardObj.issue_date.split('/')[0])
        if(month < monthNum) {
            cardObj.is_valid = false
        }
        accu.push(cardObj)
        return accu
    },[])
}

// console.log(invalidBefore(assignNewField(cards, 'is_valid'), 3))

// 6. Sort the data into ascending order of issue date.
function sortByData(cards) {
    return cards.map((card) => {
        return card
    })
    .sort((card1, card2) => {
        if(card1.issue_date.split('/')[2] === card2.issue_date.split('/')[2]) {
            if(card1.issue_date.split('/')[0] === card2.issue_date.split('/')[0]) {
                return card1.issue_date.split('/')[1] - card2.issue_date.split('/')[1]
            }
            return card1.issue_date.split('/')[0] - card2.issue_date.split('/')[0]
        }
        return card1.issue_date.split('/')[2] - card2.issue_date.split('/')[2]
    })
}

// console.log(sortByData(cards))

// 7. Group the data in such a way that we can identify which cards were assigned in which months.
function groupByMonth(cards) {
    return cards.reduce((accu, card) => {
        if(accu[card.issue_date.split('/')[0]] === undefined) {
            accu[card.issue_date.split('/')[0]] = []
        }
        accu[card.issue_date.split('/')[0]].push(card)
        return accu
    },{})
}

// console.log(groupByMonth(cards))

// function for 3 to 7 combines

function threeToSeven(data) {
    return cards.reduce((accu, card) => {
        let cardObj = {...card}
        let cvv = Math.floor(Math.random() * 1000)
        if(cvv < 100) {
            cvv += 100
        }
        cardObj['CVV'] = cvv
        accu.push(cardObj)
        return accu
    },[])
    .reduce((accu, card) => {
        let cardObj = {...card}
        cardObj['is_valid'] = true
        accu.push(cardObj)
        return accu
    },[])
    .reduce((accu, card) => {
        let cardObj = {...card}
        const month = Number.parseInt(cardObj.issue_date.split('/')[0])
        if(month < 3) {
            cardObj.is_valid = false
        }
        accu.push(cardObj)
        return accu
    },[])
    .sort((card1, card2) => {
        if(card1.issue_date.split('/')[2] === card2.issue_date.split('/')[2]) {
            if(card1.issue_date.split('/')[0] === card2.issue_date.split('/')[0]) {
                return card1.issue_date.split('/')[1] - card2.issue_date.split('/')[1]
            }
            return card1.issue_date.split('/')[0] - card2.issue_date.split('/')[0]
        }
        return card1.issue_date.split('/')[2] - card2.issue_date.split('/')[2]
    })
    .reduce((accu, card) => {
        if(accu[card.issue_date.split('/')[0]] === undefined) {
            accu[card.issue_date.split('/')[0]] = []
        }
        accu[card.issue_date.split('/')[0]].push(card)
        return accu
    },{})
}

console.log(threeToSeven(cards))